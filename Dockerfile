FROM node:18-alpine
ARG PORT=3000
WORKDIR /app
COPY install.sh /app/
RUN apk add bash
RUN chmod +x install.sh
RUN ./install.sh
#RUN wget https://github.com/johnpapa/node-hello/archive/refs/heads/master.zip && unzip -j master.zip
#CMD npm ci
CMD npm start
#CMD ping google.com

